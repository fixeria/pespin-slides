GPRS support in osmocom-bb
==========================
:author:	Pau Espin Pedrol <pespin@sysmocom.de>, Vadim Yanitskiy <vyanitskiy@sysmocom.de>
:copyright:	2024 by sysmocom - s.f.m.c. GmbH <info@sysmocom.de> (License: CC-BY-SA)
:backend:	slidy
:max-width:	54em

== Project Scope

* Introduced as part of link:https://osmocom.org/projects/baseband/wiki/MS-side_GPRS[MS-side GPRS] osmocom-bb project
* Open source GPRS "modem" implementation for OsmocomBB
* SDR GSM mobile phone (GPRS in Calpyso PHY is out of scope and not implemented)
* ARDC (Amateur Radio Digital Communications) approved grant of funds to DARC
* DARC (Deutschen Amateur-Radio-Club) contracted sysmocom to perform the related work

== Reminder: GPRS Stack

* L2 + L3: `modem`, _osmocom-bb/src/host/layer23/src/modem/_
* L1: `osmo-trx-ms-blade`, _osmo-trx/Transceiver52M/ms/_

image::gprs_stack.drawio.svg[width="40%"]

== Block diagram

* Interfaces between layers "specified" in 3GPP specs

image::ardc_darc_gprs_arch.v6.svg[width="50%"]

== libosmo-gprs: Introduction

[role="incremental"]
* Contains several .so, one per layer:
** libosmo-gprs-*: csn1, rlcmac, llc, sndcp, gmm, sm
** Helper libosmo-csn1 (the CSN.1 codec from osmo-pcu.git)
* Layer communication through primitives
* Follows 3GPP "specified" interfaces as close as possible
* Each layer can be used independently
* App in charge of forwarding primitives between layers through callback hooks
* Support multiple concurrent subscribers

== libosmo-gprs-<layer>: Public API

[role="incremental"]

* _include/osmocom/gprs/<layer>/<layer>.h_:
[role="incremental"]
** `osmo_gprs_<layer>_init(cfg)`
** `osmo_gprs_<layer>_set_log_cat(enum logc, int num)`
*** Support multiple log categories
*** Dynamic mapping of log categories done by app
** Layer related public data types

* _include/osmocom/gprs/<layer>/<layer>_prim.h_:
[role="incremental"]
** `struct osmo_gprs_<layer>_<if>_prim`: Holds all primitives for each interface
** `struct osmo_gprs_<layer>_prim`: `osmo_prim_hdr` + `struct osmo_gprs_<layer>_<if>_prim`
** `osmo_gprs_<layer>_prim_alloc()`
** `osmo_gprs_<layer>_prim_set_{up,down}_cb()`
** `osmo_gprs_<layer>_{upper_down,lower_up}()`

* _include/osmocom/gprs/<layer>/<layer>_private.h_:
* Private APIs and data types

== libosmo-gprs-sm

.SMREG:
[role="incremental"]
* Used by app directly to manage PDP Contexts
* eg. Tx PDP_ACTIVATE.req, Rx PDP_ACTIVATE.cnf

.GMMSM:
[role="incremental"]
* Trigger implicit attach/detach during PDP Context creation/deletion

== libosmo-gprs-gmm

.GMMSM:
[role="incremental"]
* Trigger implicit attach/detach during PDP Context creation/deletion

.GMMREG:
[role="incremental"]
* Explicit attach/detach by app
* SIM AUTH required towards app (Primitive osmocom specific)

.LL:
[role="incremental"]
* Rx/Tx GMM PDUs to LLC layer

.LLGMM:
[role="incremental"]
* LLC-SUSPEND, LLC-STATUS, TRIGGER (Cell Notification, READY timer)

.GMMRR:
[role="incremental"]
* Assing new negotiated TLLI to lower layers (RLC/MAC)
* Page indications
* LLC transmitted notifications (timers, counters)

== libosmo-gprs-sndcp

* Internal implementation ported from osmo-sgsn + heavy clean up

.SN:
[role="incremental"]
* Used by app to Tx/Rx user data (IP packets from tundev)
* App configures, negotiates SNDCP XIDs

.SNSM:
[role="incremental"]
* Coordination with GPRS SM layer
* eg. PDP Context was (de)activated

.LL:
[role="incremental"]
* Rx/Tx SNDCP PDUs to LLC layer

== libosmo-gprs-llc

* Internal implementation ported from osmo-sgsn + heavy clean up

.LL:
[role="incremental"]
* Forward GMM/SNDCP PDUs to/from upper layers
* LLE (per SAPI)

.LLGMM:
[role="incremental"]
* Already covered in previous slide (LLC-SUSPEND/STATUS/TRIGGER)
* LLME (shared among all SAPIs)

.GRR: (MS role)
[role="incremental"]
* Tx/Rx LLC frames to/from RLC/MAC

.BSSGP: (SGSN role)
[role="incremental"]
* Tx/Rx LLC frames to/from BSSGP

== libosmo-gprs-rlcmac

* Some parts of the implementation ported from osmo-pcu + heavy clean up

.GRR:
[role="incremental"]
* Forward LLC frames to/from upper layers (LLC)

.GMMRR:
[role="incremental"]
* Assing new negotiated TLLI to lower layers (RLC/MAC)
* Page indications
* LLC transmitted notifications (timers, counters)

.L1CTL:
[role="incremental"]
* Tx `RACH.req` (Access Burst)
* Rx `CCCH_DATA.ind` (DL CCCH/BCCH blocks)
* Tx `PDCH_{ESTABLISH,RELEASE.req` (PDCH activation/deactivation)
* Tx `CFG_{UL,DL}_TBF.req` (UL/DL TBF configuration in a PDCH)
* Rx `PDCH_DATA.{ind,cnf}`, Tx `PDCH_DATA.req` (PDCH blocks)

== `modem` Application

[role="incremental"]
* Takes care of CS management (cell sync, etc.)
** No cell scan / automatic selection, requires manual ARFCN specification
* Manages SM and MM state through `SMREG` and `GMMREG` primitive interfaces
* Manages the tundev for each APN
* Provides callbacks to forward primitives:
** Between the different libosmo-gprs-* libs
** To/from L1CTL provider (unix socket, `trxcon` + `osmo-trx-ms-blade`)

== Example: `modem` establishing a PDP Context

[role="incremental"]
. _modem_: Configure APNs (VTY)
. _modem_: Modem syncs to cell (CCCH)
. _modem -> SM_: SMREG-PDP_ACT.req
. _SM -> GMM_: GMMSM-ESTABLISH.req
. _GMM -> LLC_: LL-UNITDATA.req(GMMAttachReq)
. _LLC -> RLC/MAC_: GRR-UNITDATA.req(llc_frame[GMMAttachReq])
. _RLC/MAC_ needs a TBF to transmit data
[role="incremental"]
.. _RLC/MAC -> L1CTL_: L1CTL-RACH.req
.. _L1CTL -> RLC/MAC_: L1CTL-CCCH_DATA.ind(ImmAss[ts, fn, usf, tfi])
.. _RLC/MAC -> L1CTL_: L1CTL-PDCH_EST.req
.. _RLC/MAC -> L1CTL_: L1CTL-CFG_UL_TBF.req
.. _L1CTL -> RLC/MAC_: L1CTL-PDCH_RTS.ind(fn, ts, usf)
. _RLC/MAC -> L1CTL_: L1CTL-PDCH_DATA.req(rlc_blk[llc_frame[GMMAttachReq]])
. _L1CTL -> RLC/MAC_: L1CTL-PDCH_DATA.ind(rlc_blk[llc_frame[GMMAttachAcc]])
. _RLC/MAC -> LLC_: GRR-UNIT_DATA.req(llc_frame[GMMAttachAcc])
. _LLC -> GMM_: LL-UNIT_DATA.req(GMMAttachAcc)
. _GMM -> SM_: GMMSM-UNITDATA.req(SM-ActivatePDPContextReq)
. ...

== trxcon

*Project info*

- MS side L1 implementation (burst scheduling, Viterbi decoding)
- Started as a hobby project back in 2016
- Rx/Tx capable PoC presented at 34c3 back in 2017
* part of the SDR PHY for osmocom-bb initiative
* worked with `grgsm_trx`, GNU Radio gr-gsm based transceiver
- Actively used by `ttcn3-bts-test` (conformance/regression tests for osmo-bts)

----
commit 8975b437ed476a1655e19b68d113bc93c468b087
Author: Vadim Yanitskiy <axilirator@gmail.com>
Date:   Thu Jun 30 00:41:58 2016 +0600

    host/trxcon: introduce a new 'trxcon' application
----

== trxcon

*Project architecture (before GPRS MS)*

image::habr.png[float="left"]

- Very simple proxy between the `L1CTL` and `TRXC`/`TRXD` interfaces
- No clear component separation, direct function calls
- No state tracking (FSMs), no timeouts...
- PoC level code quality

== trxcon

*Project architecture (after GPRS MS)*

image::trxcon_arch.drawio.svg[width="40%"]

- Proper separation between components
* libraries: `libtrxcon`, `libl1sched`, `l1gprs`
* ... can be re-used by other apps
- `trxcon_fsm` - the "heart" and the "brain"
- `libgprs`: GPRS support!

== trxcon / trxcon_fsm

*Event flow*

.Cell selection
[mscgen]
----
msc {
	width="2048", arcgradient="10";

	L23APP [label="l23_app"],
	L1CTLSRV [label="l1ctl_server"],
	FSM [label="trxcon_fsm"],
	SCHED [label="l1sched"],
	PHYIF [label="phyif"];

	FSM box FSM [label="TRXCON_ST_RESET"];
	L23APP => L1CTLSRV [label="L1CTL_FBSB_REQ"];
	L1CTLSRV => FSM [label="TRXCON_EV_FBSB_SEARCH_REQ"];
	FSM box FSM [label="TRXCON_ST_FBSB_SEARCH"];
	FSM => SCHED [label="l1sched_configure_ts(0)"];
	FSM <= SCHED [label="TRXCON_EV_SET_PHY_CONFIG_REQ"];
	FSM => PHYIF [label="TRXCON_PHYIF_CMDT_SETSLOT"];
	FSM => PHYIF [label="TRXCON_PHYIF_CMDT_SETFREQ_H0"];
	FSM => PHYIF [label="TRXCON_PHYIF_CMDT_POWERON"];
	|||;
	PHYIF => SCHED [label="trxcon_phyif_handle_burst_ind()"];
	PHYIF => SCHED [label="trxcon_phyif_handle_burst_ind()"];
	PHYIF => SCHED [label="trxcon_phyif_handle_burst_ind()"];
	SCHED => FSM [label="TRXCON_EV_FBSB_SEARCH_RES"];
	FSM box FSM [label="TRXCON_ST_BCCH_CCCH"];
	L1CTLSRV <= FSM [label="l1ctl_tx_fbsb_conf()"];
	L23APP <= L1CTLSRV [label="L1CTL_FBSB_CONF"];
}
----

== trxcon / trxcon_fsm

*Event flow*

.DCCH establishment & release
[mscgen]
----
msc {
	width="2048", arcgradient="10";

	L23APP [label="l23_app"],
	L1CTLSRV [label="l1ctl_server"],
	FSM [label="trxcon_fsm"],
	SCHED [label="l1sched"],
	PHYIF [label="phyif"];

	FSM box FSM [label="TRXCON_ST_BCCH_CCCH"];
	L23APP => L1CTLSRV [label="L1CTL_DM_EST_REQ"];
	L1CTLSRV => FSM [label="TRXCON_EV_DCH_EST_REQ"];

	FSM => PHYIF [label="TRXCON_PHYIF_CMDT_SETFREQ_{H0,H1}"];
	FSM => SCHED [label="l1sched_reset()"];
	FSM <= SCHED [label="TRXCON_EV_SET_PHY_CONFIG_REQ"];
	FSM => PHYIF [label="TRXCON_PHYIF_CMDT_SETSLOT"];
	FSM box FSM [label="TRXCON_ST_DEDICATED"];
	|||;
	...;
	|||;

	L23APP => L1CTLSRV [label="L1CTL_DM_REL_REQ"];
	L1CTLSRV => FSM [label="TRXCON_EV_DCH_REL_REQ"];
	FSM => SCHED [label="l1sched_reset(): type=SCHED"];
	FSM <= SCHED [label="TRXCON_EV_SET_PHY_CONFIG_REQ"];
	FSM => PHYIF [label="TRXCON_PHYIF_CMDT_SETSLOT"];
	|||;
	L23APP => L1CTLSRV [label="L1CTL_RESET_REQ (type=FULL)"];
	FSM box FSM [label="TRXCON_ST_RESET"];
	L1CTLSRV => FSM [label="TRXCON_EV_RESET_FULL_REQ"];
	FSM => SCHED [label="l1sched_reset(): type=FULL"];
	FSM => PHYIF [label="TRXCON_PHYIF_CMDT_RESET"];
}
----

== trxcon / trxcon_fsm

*Event flow*

.DCCH burst Rx/Tx
[mscgen]
----
msc {
	width="2048", arcgradient="10";

	L23APP [label="l23_app"],
	L1CTLSRV [label="l1ctl_server"],
	FSM [label="trxcon_fsm"],
	SCHED [label="l1sched"],
	PHYIF [label="phyif"];

	FSM box FSM [label="TRXCON_ST_DEDICATED"];
	PHYIF => SCHED [label="trxcon_phyif_handle_burst_ind(): bid=0"];
	PHYIF => SCHED [label="trxcon_phyif_handle_burst_ind(): bid=1"];
	PHYIF => SCHED [label="trxcon_phyif_handle_burst_ind(): bid=2"];
	PHYIF => SCHED [label="trxcon_phyif_handle_burst_ind(): bid=3"];
	SCHED note SCHED [label="3GPP TS 45.003 decoding"];
	SCHED => FSM [label="TRXCON_EV_RX_DATA_IND"];
	L1CTLSRV <= FSM [label="l1ctl_tx_dt_ind()"];
	L23APP <= L1CTLSRV [label="L1CTL_DATA_IND"];
	|||;

	L23APP => L1CTLSRV [label="L1CTL_DATA_REQ"];
	L1CTLSRV => FSM [label="TRXCON_EV_TX_DATA_REQ"];
	FSM => SCHED [label="l1sched_prim_from_user(): L1SCHED_PRIM_T_DATA.Req"];
	SCHED note SCHED [label="3GPP TS 45.003 encoding"];
	|||;

	PHYIF => FSM [label="trxcon_phyif_handle_rts_ind(): TDMA Fn/Tn"];
	FSM => SCHED [label="l1sched_pull_burst(): TDMA Fn/Tn"];
	PHYIF <= SCHED [label="trxcon_phyif_handle_burst_req(): bid=0"];
	PHYIF => FSM [label="trxcon_phyif_handle_rts_ind(): TDMA Fn/Tn"];
	FSM => SCHED [label="l1sched_pull_burst(): TDMA Fn/Tn"];
	PHYIF <= SCHED [label="trxcon_phyif_handle_burst_req(): bid=1"];
	PHYIF => FSM [label="trxcon_phyif_handle_rts_ind(): TDMA Fn/Tn"];
	FSM => SCHED [label="l1sched_pull_burst(): TDMA Fn/Tn"];
	PHYIF <= SCHED [label="trxcon_phyif_handle_burst_req(): bid=2"];
	PHYIF => FSM [label="trxcon_phyif_handle_rts_ind(): TDMA Fn/Tn"];
	FSM => SCHED [label="l1sched_pull_burst(): TDMA Fn/Tn"];
	PHYIF <= SCHED [label="trxcon_phyif_handle_burst_req(): bid=3"];
}
----

== trxcon / l1gprs

*Performance requirements*

- Unlike GSM, GPRS imposes tight performance requirements
* an `UL BLOCK.req` *must* be sent at specific Tn/Fn, not earlier or later
- TDMA gives us 5.77ms to handle a `DL BLOCK.ind` and prepare `UL BLOCK.req`
* see https://people.osmocom.org/fixeria/pdch.txt
- Solution 1: move the whole RLC/MAC to the lower layers
- Solution 2: move part of RLC/MAC to the lower layers
* we decided to follow this approach
* the part of RLC/MAC is `l1gprs`

== trxcon / l1gprs

*The l1gprs library*

- RLC/MAC (*lower* part) as per 3GPP TS 44.060
- Intentionally separate library with abstract API
* already used by the `virtphy` app (might be broken, though)
* can _hopefully_ be integrated with the `layer1` firmware for Calypso
** ancient copy of `libosmocore.git` is a blocker (see `OS#2378`)
- Relevant files: `src/shared/l1gprs.c` and `include/l1gprs.h`
- Depends on `libosmocore` and `libosmogsm`

image::l1gprs.drawio.svg[width="80%"]

== trxcon / l1gprs

*New L1CTL messages*

- `L1CTL_GPRS_{UL,DL}_TBF_CFG_REQ` - UL/DL TBF configuration Req
- `L1CTL_GPRS_{UL,DL}_BLOCK_{REQ,IND}` - UL/DL PDTCH block Req/Ind
- `L1CTL_GPRS_RTS_IND` - Ready-to-Send Ind (from PHY)
- `L1CTL_GPRS_UL_BLOCK_CNF` - UL PDTCH block Confirmation

*API*

- `l1gprs_logging_init(logc)`
- `l1gprs_state_alloc()` / `l1gprs_state_free()`
- `l1gprs_handle_{ul,dl}_tbf_cfg_req()`
- `l1gprs_handle_ul_block_{req,cnf}()`
- `l1gprs_handle_dl_block_ind()`
- `l1gprs_handle_rts_ind()`

== trxcon / l1gprs

*Event flow*

.PDCH block Rx/Tx
[mscgen]
----
msc {
	width="2048";

	L23APP [label="l23_app"],
	L1CTLSRV [label="l1ctl_server"],
	FSM [label="trxcon_fsm"],
	SCHED [label="l1sched"],
	GPRS [label="l1gprs"];

	FSM box FSM [label="TRXCON_ST_PACKET_DATA"];
	L23APP => L1CTLSRV [label="L1CTL_GPRS_UL_TBF_CFG_REQ"];
	L1CTLSRV => FSM [label="TRXCON_EV_GPRS_UL_TBF_CFG_REQ"];
	FSM => GPRS [label="l1gprs_handle_ul_tbf_cfg_req(msgb)"];
	|||;
	...;
	|||;
	SCHED box SCHED [label="Rx PDTCH burst (bid=0)"];
	SCHED box SCHED [label="Rx PDTCH burst (bid=1)"];
	SCHED box SCHED [label="Rx PDTCH burst (bid=2)"];
	SCHED box SCHED [label="Rx PDTCH burst (bid=3)"];
	SCHED => FSM [label="TRXCON_EV_RX_DATA_IND (PDTCH)"];
	FSM => GPRS [label="msg = l1gprs_handle_dl_block_ind()"];
	FSM => L1CTLSRV [label="trxcon_l1ctl_send(msg)"];
	L1CTLSRV => L23APP [label="L1CTL_GPRS_DL_BLOCK_IND"];
	|||;
	FSM => GPRS [label="msg = l1gprs_handle_rts_ind(Fn++)"];
	FSM => L1CTLSRV [label="trxcon_l1ctl_send(msg)"];
	L1CTLSRV => L23APP [label="L1CTL_GPRS_RTS_IND"];
	|||;
	...;
	|||;
	L1CTLSRV <= L23APP [label="L1CTL_GPRS_UL_BLOCK_REQ"];
	FSM <= L1CTLSRV [label="TRXCON_EV_GPRS_UL_BLOCK_REQ"];
	FSM => GPRS [label="l1gprs_handle_ul_block_req(msg)"];
	FSM => SCHED [label="l1sched_prim_from_user(): L1SCHED_PRIM_T_DATA.Req"];
	|||;
	...;
	|||;
	SCHED box SCHED [label="Tx PDTCH burst (bid=0)"];
	SCHED box SCHED [label="Tx PDTCH burst (bid=1)"];
	SCHED box SCHED [label="Tx PDTCH burst (bid=2)"];
	SCHED box SCHED [label="Tx PDTCH burst (bid=3)"];
	FSM <= SCHED [label="TRXCON_EV_TX_DATA_CNF"];
	FSM => GPRS [label="msg = l1gprs_handle_ul_block_cnf()"];
	FSM => L1CTLSRV [label="trxcon_l1ctl_send(msg)"];
	L1CTLSRV => L23APP [label="L1CTL_GPRS_UL_BLOCK_CNF"];
}
----

== osmo-trx-ms

*The MS side transceiver*

- `osmo-trx` was so far the BTS side transceiver
* `ms` branch: early/unfinished work by Tom Tsou (2014)
- `osmo-trx-ms` the MS side transceiver by `@Hoernchen`
* partly based on the work by Tom Tsou
* partly based on burst detector from gr-gsm by Piotr Krysik
* based on `libtrxcon` + `libl1sched` + `libl1gprs`
* *requires* the clock source to work

*Limitations*

- SDR support: bladeRF only
* `-blade` variant only, `-uhd` is untested (likely broken)
- no VTY interface, hard-coded logging config
- may hang/crash sometimes, requires restart

== osmo-trx-ms

*How to build?*

[source,shell]
----
$ git clone https://git.osmocom.org/osmo-trx
$ git submodule init # <1>
$ git submodule update # <1>
$ autoreconf -fi
$ ./configure --with-mstrx --with-bladerf
----
<1> Fetch the osmocom-bb git submodule

*Debian package(s)*

- https://osmocom.org/projects/cellular-infrastructure/wiki/Nightly_Builds
- https://obs.osmocom.org/project/show/osmocom:nightly
- https://downloads.osmocom.org/packages/osmocom:/nightly/

[source,shell]
----
$ apt install osmo-trx-ms-blade
----

== Future work

* libosmo-gprs:
** Cleanup API: No need 1 alloc API per primitive+operation type
** Declare its API stable

* libosmo-gprs-rlcmac:
** Support TBFs with >1 TS
** Support EGPRS
** Tons of other features missing

* osmo-sgsn:
** WIP: Port to libosmo-gprs-{llc,sndcp}, link:https://osmocom.org/issues/5853[OS#5853] (branch `pespin/libosmo-gprs`)

== Know more

* link:https://osmocom.org/projects/baseband/wiki/MS-side_GPRS[MS-side GPRS]
* link:https://osmocom.org/projects/libosmo-gprs[libosmo-gprs Redmine Project]
